FROM python:3-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    rsync

WORKDIR /usr/src/app

COPY ./ .

RUN pip install --no-cache-dir . \
    && pip install --no-cache-dir raven

CMD ['archive']
