import os
from pathlib import Path

from invoke import task

DBHOST = os.environ.get("DBHOST", "127.0.0.1")


@task
def lint(c):
    """Linting code"""
    c.run("pre-commit install --install-hooks")
    c.run("pre-commit run --all-files --show-diff-on-failure")


@task
def init_test(c):
    """Install test dependencies"""
    c.run("pip install -r requirements.tests.txt")
    c.run("pip install -e .")


@task(pre=[init_test])
def init_dev(c):
    """Init dev env with dev dependencies"""
    c.run("pip install -r requirements.dev.txt")
    c.run("pre-commit install --install-hooks")


@task
def test(c):
    """Launch pytest"""
    c.run("pytest")


@task(pre=[init_dev])
def build(c):
    """build tarball and wheel"""
    c.run("rm -rf dist/")
    c.run("python setup.py sdist bdist_wheel")
    c.run("twine check dist/*")
    c.run("pip install dist/*.whl")
    c.run("archive --help")
    c.run("pip install -e .")


@task(post=[build])
def release(c, part):
    """Bump version part (major.minor.patch) and build"""
    c.run("bump2version", part)


@task
def push_on_pypi(c):
    """push to any wheel house"""
    c.run("twine --version")
    c.run("twine upload --verbose dist/*")


@task()
def clean(c):
    """clean"""
    c.run("docker rm -fv pgtest1")
    c.run("docker rm -fv pgtest2")
    c.run("docker rm -fv filestore")
    c.run("rm -fr dist/")
    c.run("rm -fr bkp/")
    c.run("rm -fr filestore/")


@task(pre=[clean])
def integration_pgsql(c, dev=False):
    """Launch pgsl integration test"""
    c.run(
        """
      docker run -d \
        --name pgtest1 \
        -l archiver.isactive \
        -l archiver.username=abc \
        -l archiver.dbname=dbtest \
        -l archiver.environment=test \
        -l archiver.prefix=cool \
        -e POSTGRES_DB=dbtest \
        -e POSTGRES_USER=abc \
        -e POSTGRES_PASSWORD=abcd \
        postgres:alpine
    """
    )
    c.run(
        "docker exec pgtest1 sh -c  'while ! psql -U abc -d dbtest -c \"select 1\"; do sleep 1; done;'"
    )
    c.run(
        "docker exec pgtest1 psql -d dbtest -U abc -c 'CREATE TABLE test_table (test_field integer); INSERT INTO test_table VALUES (15);'"
    )
    c.run("mkdir -p bkp")
    if dev:
        c.run("export ARCHIVER_LABEL_PREFIX=archiver; archive -r -d ./bkp")
    else:
        c.run(
            "docker run --rm -v $PWD/bkp:/backups -e ARCHIVER_LABEL_PREFIX=archiver -v /var/run/docker.sock:/var/run/docker.sock $IMAGE:$COMMIT_TAG archive -r"
        )
    c.run(
        "docker run -d --name pgtest2 -e POSTGRES_DB=restore_dbtest -e POSTGRES_USER=restore_user -e POSTGRES_PASSWORD=password postgres:alpine"
    )
    c.run(
        "docker exec pgtest2 sh -c  'while ! psql -U restore_user -d restore_dbtest -c \"select 1\"; do sleep 1; done;'"
    )
    c.run("docker cp bkp/pgtest1/test/cooldbtest-* pgtest2:/tmp/backup.dump")
    c.run(
        "docker exec pgtest2 pg_restore -U restore_user -Fc -O -d restore_dbtest /tmp/backup.dump"
    )
    res = c.run(
        "docker exec pgtest2 psql -A -d restore_dbtest -U restore_user -t -c 'SELECT sum(test_field) FROM test_table;'"
    )
    if not res.ok or (res.ok and int(res.stdout) != 15):
        print(f"unexpected restored database: {str(res)}")
        raise RuntimeError(res)
    res = c.run("docker exec pgtest1 ls /tmp/ | wc -l")
    if not res.ok or (res.ok and int(res.stdout) != 0):
        print(f"Expected removed dump: {str(res)}")
        raise RuntimeError(res)


@task(pre=[clean])
def integration_rsync(c, dev=False):
    """Launch rsunc integration test"""
    c.run("mkdir -p filestore")
    c.run("echo 'test' > filestore/file.txt")
    c.run(
        """
        docker run -d \
            --name filestore \
            -v $PWD/filestore:/files/in/container \
            -v $PWD/filestore/file.txt:/ingore/mounted/file \
            -v archiver-test-volume:/tmp/test \
            -l archiver.isactive \
            -l archiver.driver=rsync \
            -l archiver.environment=dev \
            nginx:alpine
        """
    )
    c.run("docker exec filestore sh -c 'echo testvol > /tmp/test/file2.txt'")
    if dev:
        c.run("export ARCHIVER_LABEL_PREFIX=archiver; archive -r -d ./bkp")
    else:
        c.run(
            """
            docker run --rm \
                -v $PWD/bkp:/backups \
                -v /:/hostfs \
                -e ARCHIVER_LABEL_PREFIX=archiver \
                -v /var/run/docker.sock:/var/run/docker.sock \
                $IMAGE:$COMMIT_TAG archive --host-fs /hostfs -r
            """
        )

    with open("bkp/filestore/dev/files-in-container/file.txt", "r") as f:
        backup_result = f.read().strip()

    assert backup_result == "test", f"File wasn't rsync properly got {backup_result}"

    with open("bkp/filestore/dev/tmp-test/file2.txt", "r") as f:
        backup_result = f.read().strip()

    assert backup_result == "testvol", f"File wasn't rsync properly got {backup_result}"

    assert Path("/bkp/filestore/dev/ingore-mounted-file").is_file() is False
