import os
import subprocess
from pathlib import Path
from unittest import TestCase, mock

import docker
from mock import MagicMock
from testfixtures import Replace, TempDirectory, test_datetime

from archiver.archive import (
    DEFAULT_LABEL_PREFIX,
    Archiver,
    MysqlDriver,
    PgsqlDriver,
    RsyncDriver,
    backup,
)


@mock.patch("docker.DockerClient")
@mock.patch("archiver.archive.Archiver.backup_all")
def test_entrypoints(backup_all, docker_cli):
    with mock.patch(
        "sys.argv",
        [
            "prog-name",
            "-l",
            "INFO",
            "--host-fs",
            "/hfs",
            "-d",
            "/bck_dir",
            "-r",
            "-s",
            '{"label": "test"}',
        ],
    ):
        archiver = backup(testing=True)
        backup_all.assert_called_once()
        docker_cli.assert_called_once()
        assert archiver._root_directory == "/bck_dir"
        assert archiver._selector == {"label": "test"}
        assert archiver.host_fs == "/hfs"
        assert archiver._raise is True


class TestPgsqlDriver(TestCase):
    def setUp(self):
        def restore_untar(previous_method):
            PgsqlDriver.untar = previous_method

        def fake_untar(self, path):
            path, _ = os.path.splitext(path)
            Path(path).touch()
            return path

        olduntar = PgsqlDriver.untar
        self.addCleanup(restore_untar, olduntar)
        PgsqlDriver.untar = fake_untar
        self.init_mock()

    def init_mock(self):
        self.container_mock = MagicMock(
            **{
                "labels": {
                    f"{DEFAULT_LABEL_PREFIX}.isactive": "",
                    f"{DEFAULT_LABEL_PREFIX}.driver": "pgsql",
                    f"{DEFAULT_LABEL_PREFIX}.dbname": "myunitestdb",
                    f"{DEFAULT_LABEL_PREFIX}.project": "myunitestproject",
                    f"{DEFAULT_LABEL_PREFIX}.username": "myunitestuser",
                },
                "exec_run.return_value": MagicMock(exit_code=0, output=b""),
                "get_archive.return_value": ([], MagicMock()),
            }
        )
        self.docker_mock = MagicMock(
            **{
                "containers.info.return_value": {
                    "Swarm": {"LocalNodeState": "inactive"}
                },
                "containers.list.return_value": [
                    self.container_mock,
                ],
            }
        )
        # you can change this default mock behaviour in test case likes
        # self.docker_mock.configure_mock()
        # self.container_mock.configure_mock()

    def test_instanciate_archiver(self):
        """A simple test case to start testing project"""
        archiver = Archiver(docker_cli=self.docker_mock)
        self.assertTrue(archiver)

    def test_instanciate_pgsql_driver(self):
        """A simple test case to start testing project"""
        driver = PgsqlDriver(None)
        self.assertTrue(driver)

    def test_instanciate_mysql_driver(self):
        """A simple test case to start testing project"""
        driver = MysqlDriver(None)
        self.assertTrue(driver)

    def test_backup_all(self):
        with TempDirectory() as d:
            archiver = Archiver(docker_cli=self.docker_mock, root_directory=d.path)

            with Replace(
                "archiver.archive.datetime",
                test_datetime(1984, 6, 15, 12, 11, 10, delta=0, delta_type="seconds"),
            ):
                self.assertEqual(
                    archiver.backup_all(),
                    [
                        os.path.join(
                            d.path,
                            "myunitestproject/"
                            "myunitestdb-1984-06-15T121110.000000.sql.gz",
                        )
                    ],
                )

    def test_backup_command_fail(self):
        self.container_mock.configure_mock(
            **{
                "exec_run.return_value": MagicMock(
                    exit_code=1, output=b"testing backup failed"
                ),
            }
        )
        with TempDirectory() as d:
            archiver = Archiver(
                docker_cli=self.docker_mock,
                root_directory=d.path,
                raise_on_failure=False,
            )
            self.assertEqual([], archiver.backup_all())

    def test_something_goes_wrong(self):
        self.container_mock.configure_mock(
            **{
                "exec_run.return_value": None,
                "exec_run.side_effect": docker.errors.APIError("Mock side effect"),
            }
        )
        with TempDirectory() as d:
            archiver = Archiver(
                docker_cli=self.docker_mock,
                root_directory=d.path,
                raise_on_failure=False,
            )
            self.assertEqual([], archiver.backup_all())

    def test_raise(self):
        self.container_mock.configure_mock(
            **{
                "exec_run.return_value": None,
                "exec_run.side_effect": docker.errors.APIError("Mock side effect"),
            }
        )
        with TempDirectory() as d:
            with self.assertRaises(docker.errors.APIError):
                archiver = Archiver(
                    docker_cli=self.docker_mock,
                    root_directory=d.path,
                    raise_on_failure=True,
                )
                archiver.backup_all()


class TestRsyncDriver(TestCase):
    def get_container_mock(self, attrs=None, extra_labels=None):
        labels = {
            f"{DEFAULT_LABEL_PREFIX}.isactive": "",
            f"{DEFAULT_LABEL_PREFIX}.driver": "rsync",
        }
        if extra_labels is not None:
            labels.update(extra_labels)
        if attrs is None:
            attrs = {
                "Mounts": [
                    {
                        "Type": "bind",
                        "Source": "/home/user/tmp",
                        "Destination": "/tmp/other",
                        "RW": True,
                    },
                    {
                        "Type": "bind",
                        "Source": "/tmp",
                        "Destination": "/somewhere",
                        "RW": True,
                    },
                    {
                        "Type": "bind",
                        "Source": "/afile",
                        "Destination": "/afile",
                        "RW": True,
                    },
                    {
                        "Type": "volume",
                        "Driver": "local",
                        "Name": "toto",
                        "Source": "/var/lib/docker/volumes/toto/_data",
                        "Destination": "/tmp/test",
                        "RW": True,
                    },
                    {
                        "Type": "volume",
                        "Driver": "local",
                        "Name": "test-ro",
                        "Source": "/var/lib/docker/volumes/toto/_data",
                        "Destination": "/tmp/readonly",
                        "Mode": "ro",
                        "RW": False,
                    },
                ]
            }
        return MagicMock(
            **{
                "labels": labels,
                "attrs": attrs,
            }
        )

    @mock.patch(
        "archiver.archive.Path.is_dir",
        side_effect=[True, True, False, True, True, True, True],
    )
    @mock.patch("pathlib.Path.mkdir")
    @mock.patch("subprocess.run")
    def test_rsync_backup(self, run, _, _1):
        driver = RsyncDriver(
            self.get_container_mock(
                extra_labels={
                    f"{DEFAULT_LABEL_PREFIX}.project": "pjct",
                    f"{DEFAULT_LABEL_PREFIX}.prefix": "PREF-",
                    f"{DEFAULT_LABEL_PREFIX}.environment": "DEV",
                    f"{DEFAULT_LABEL_PREFIX}.rsync-params": "--other-param",
                },
            ),
            host_fs="/myhostfs",
        )
        driver.retreive_container_labels()
        assert driver.backup() == [
            "/backups/pjct/DEV/PREF-tmp-other",
            "/backups/pjct/DEV/PREF-somewhere",
            "/backups/pjct/DEV/PREF-tmp-test",
        ]
        assert run.mock_calls == [
            mock.call(
                "rsync -avhP --delete --other-param "
                "/myhostfs/home/user/tmp/ "
                "/backups/pjct/DEV/PREF-tmp-other",
                shell=True,
                check=True,
                capture_output=True,
            ),
            mock.call(
                "rsync -avhP --delete --other-param /myhostfs/tmp/ "
                "/backups/pjct/DEV/PREF-somewhere",
                shell=True,
                check=True,
                capture_output=True,
            ),
            mock.call(
                "rsync -avhP --delete --other-param "
                "/myhostfs/var/lib/docker/volumes/toto/_data/ "
                "/backups/pjct/DEV/PREF-tmp-test",
                shell=True,
                check=True,
                capture_output=True,
            ),
        ]

    @mock.patch(
        "archiver.archive.Path.is_dir", side_effect=[True, True, False, True, True]
    )
    def test_retreive_container_info(self, _):
        driver = RsyncDriver(
            self.get_container_mock(
                extra_labels={f"{DEFAULT_LABEL_PREFIX}.ro-volumes": "no"}
            )
        )
        driver.retreive_container_labels()
        self.assertListEqual(
            driver.folders,
            [
                (
                    "tmp-other",
                    "/home/user/tmp",
                ),
                (
                    "somewhere",
                    "/tmp",
                ),
                (
                    "tmp-test",
                    "/var/lib/docker/volumes/toto/_data",
                ),
            ],
        )

    @mock.patch(
        "archiver.archive.Path.is_dir", side_effect=[True, True, False, True, True]
    )
    def test_retreive_container_info_with_ro(self, _):
        driver = RsyncDriver(
            self.get_container_mock(
                extra_labels={f"{DEFAULT_LABEL_PREFIX}.ro-volumes": "yes"}
            ),
            host_fs="/myhostfs",
        )
        driver.retreive_container_labels()
        self.assertListEqual(
            driver.folders,
            [
                (
                    "tmp-other",
                    "/myhostfs/home/user/tmp",
                ),
                (
                    "somewhere",
                    "/myhostfs/tmp",
                ),
                (
                    "tmp-test",
                    "/myhostfs/var/lib/docker/volumes/toto/_data",
                ),
                (
                    "tmp-readonly",
                    "/myhostfs/var/lib/docker/volumes/toto/_data",
                ),
            ],
        )

    def test_retreive_container_info_without_mount(self):
        driver = RsyncDriver(
            MagicMock(
                **{
                    "labels": {
                        f"{DEFAULT_LABEL_PREFIX}.isactive": "",
                        f"{DEFAULT_LABEL_PREFIX}.driver": "rsync",
                    },
                    "attrs": {},
                }
            )
        )
        with self.assertRaisesRegex(
            ValueError, ".*Couldn't find local volume nor bind to rsync.*"
        ):
            driver.retreive_container_labels()

    @mock.patch(
        "archiver.archive.Path.is_dir",
        side_effect=[True, True, False, True, True, True, True],
    )
    @mock.patch("subprocess.run")
    def test_backup_all(self, _, _1):
        with TempDirectory() as d:
            archiver = Archiver(
                docker_cli=MagicMock(
                    **{
                        "containers.info.return_value": {
                            "Swarm": {"LocalNodeState": "inactive"}
                        },
                        "containers.list.return_value": [
                            self.get_container_mock(
                                extra_labels={
                                    f"{DEFAULT_LABEL_PREFIX}.project": "pjct",
                                }
                            ),
                        ],
                    }
                ),
                root_directory=d.path,
            )
            self.assertEqual(
                archiver.backup_all(),
                [
                    f"{d.path}/pjct/tmp-other",
                    f"{d.path}/pjct/somewhere",
                    f"{d.path}/pjct/tmp-test",
                ],
            )
            assert Path(f"{d.path}/pjct").is_dir() is True

    @mock.patch(
        "subprocess.run",
        side_effect=subprocess.CalledProcessError(cmd="test", returncode=33),
    )
    def test_backup_all_raise(self, _):
        with TempDirectory() as d:
            archiver = Archiver(
                docker_cli=MagicMock(
                    **{
                        "containers.info.return_value": {
                            "Swarm": {"LocalNodeState": "inactive"}
                        },
                        "containers.list.return_value": [
                            self.get_container_mock(
                                extra_labels={
                                    f"{DEFAULT_LABEL_PREFIX}.project": "pjct",
                                }
                            ),
                        ],
                    }
                ),
                root_directory=d.path,
            )
            with self.assertRaisesRegex(
                subprocess.CalledProcessError,
                ".*Command 'test' returned non-zero exit status 33.*",
            ):
                archiver.backup_all(),
